Group Members: Rossi Palomba






Selection Title: finalselection.wav






Score Format:
<?xml version="1.0" encoding="utf-8"?>
<score bpm="120" beatspermeasure="2">
    <instrument instrument="PianoInstrument">
        <note measure="1" beat="1" note="A0#l" pedal="false" speed="1"/>
        …
        …
    </instrument>
</score>


Bpm = Beats per minute
Beatspermeasure = Beats per measure
Instrument = Instrument
Note:
Measure = Measure
Beat = Beat of measure
Note = Piano note / pedal note
Pedal = If pedal is pressed down
Speed = Speed in which key is pressed; Greater the number the faster the press.
